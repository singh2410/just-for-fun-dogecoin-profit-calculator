#!/usr/bin/env python
# coding: utf-8

# # Just for fun: Building a Dogecoin Profit Calculator
# #By- Aarush Kumar
# #Date- May 12,2021

# In[6]:


import numpy as np
import pandas as pd


# In[7]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Dogecoin Profit Calculator/DOGE-USD.csv')


# In[8]:


df


# In[9]:


df=df.set_index(pd.DatetimeIndex(df['Date'].values))
df


# In[10]:


amount_invested=input('Enter amount invested in Dollars: ')
print(amount_invested)
invest_date=input('Enter the date that you invested: ')
print(invest_date)


# In[11]:


col1= 'Low'
col2= 'High'
#Getting assests for day that user invested
price1= df[col1][invest_date]
price2= df[col2][invest_date]
#quantity of assests user will have
quantity1 =int(amount_invested)/price1
quantity2 =int(amount_invested)/price2
#computing the profit range
profit1=(quantity1 * df[col1][-1]) - int(amount_invested)
profit2=(quantity2 * df[col2][-1]) - int(amount_invested)
print('Congrats! You have made between $',round(profit1, 2), 'and $',round(profit2, 2), 'as of',df['Date'][-1])


# In[12]:


#Range of Investment Return
RIR1= profit1 / int(amount_invested)*100
RIR2= profit2 / int(amount_invested)*100
print('Your range of investment return (RIR) would be between', round(RIR1, 2),'% and', round(RIR2, 2), '% as of', df['Date'][-1])

